#!/bin/bash

if [ "$1" == "" ] ; then
  echo -e "Enter your commit message\n"
  read message
else 
  message=$@
fi

git pull &>/dev/null
git fetch &>/dev/null

git add -A
git commit -m "$message"
git push -u origin master

